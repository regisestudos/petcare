import { Component } from '@angular/core';
import { NavController, IonicPage, MenuController } from 'ionic-angular';

//essa notação diz que eu posso referenciar essa classe pelo nome dela entre aspas
@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController, public menu: MenuController) {

  }

  //esse nome de metodo é exatamente o mesmo nome dos eventos de ciclo de vida do ionic, com isso ele reconhece o evente e excecuta a impementação
  ionViewWillEnter(){
    this.menu.swipeEnable(false);
  }

  ionViewDidLeave(){
    this.menu.swipeEnable(true);
  }

  login(){
    //esse metodo empilha essa pagina em cima de outra
    // this.navCtrl.push('CategoriasPage');
    //esse nageva para a pagina
    this.navCtrl.setRoot('CategoriasPage');
  }

}
